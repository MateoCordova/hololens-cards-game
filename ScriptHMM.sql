USE [MonsterMaze]
GO
/****** Object:  Table [dbo].[tblBaraja]    Script Date: 3/6/2019 7:31:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBaraja](
	[CodBaraja] [int] IDENTITY(1,1) NOT NULL,
	[Estado] [varchar](15) NOT NULL,
	[FechaCreacion] [date] NULL,
	[Victorias] [int] NULL,
	[ID_Usuario] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[CodBaraja] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
/****** Object:  Table [dbo].[tblBarajaCarta]    Script Date: 3/6/2019 7:31:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBarajaCarta](
	[CodBaraja] [int] NULL,
	[IDCarta] [int] NULL,
	[numerocarta] [int] NULL
)
GO
/****** Object:  Table [dbo].[tblCarta]    Script Date: 3/6/2019 7:31:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCarta](
	[IDCarta] [int] IDENTITY(1,1) NOT NULL,
	[CostoMana] [int] NOT NULL,
	[NombreCarta] [varchar](50) NULL,
	[Descripcion] [varchar](500) NULL,
	[PV] [int] NULL,
	[Ataque] [int] NULL,
	[Movilidad] [int] NULL,
	[tipo] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[IDCarta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
/****** Object:  Table [dbo].[tblPartida]    Script Date: 3/6/2019 7:31:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPartida](
	[CodPartida] [int] IDENTITY(1,1) NOT NULL,
	[FechaCreacion] [date] NULL,
	[Ganador] [varchar](50) NOT NULL,
	[Perdedor] [varchar](50) NOT NULL,
	[Duracion] [time](7) NULL,
	[ID_Usuario] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[CodPartida] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
/****** Object:  Table [dbo].[tblusuario]    Script Date: 3/6/2019 7:31:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblusuario](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NULL,
	[NombreUsuario] [varchar](50) NOT NULL,
	[Correo] [varchar](50) NOT NULL,
	[passwd] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
SET IDENTITY_INSERT [dbo].[tblBaraja] ON 

INSERT [dbo].[tblBaraja] ([CodBaraja], [Estado], [FechaCreacion], [Victorias], [ID_Usuario]) VALUES (4, N'Lleno', CAST(N'2019-05-31' AS Date), 0, 28)
INSERT [dbo].[tblBaraja] ([CodBaraja], [Estado], [FechaCreacion], [Victorias], [ID_Usuario]) VALUES (5, N'Vacia', CAST(N'2019-05-31' AS Date), 0, 28)
INSERT [dbo].[tblBaraja] ([CodBaraja], [Estado], [FechaCreacion], [Victorias], [ID_Usuario]) VALUES (6, N'Lleno', CAST(N'2019-05-31' AS Date), 0, 28)
SET IDENTITY_INSERT [dbo].[tblBaraja] OFF
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 28, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 29, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 30, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 31, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 32, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 33, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 34, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 35, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 36, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 37, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 38, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 39, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 40, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 41, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 42, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 43, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (4, 28, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (4, 29, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (4, 31, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (4, 32, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (4, 34, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (4, 41, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (4, 42, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (4, 44, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (4, 46, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (4, 47, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (4, 33, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (4, 39, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 28, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 29, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 30, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 31, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 32, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 33, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 34, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 35, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 36, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 37, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 38, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 39, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 40, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 41, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 42, 1)
INSERT [dbo].[tblBarajaCarta] ([CodBaraja], [IDCarta], [numerocarta]) VALUES (6, 43, 1)
SET IDENTITY_INSERT [dbo].[tblCarta] ON 

INSERT [dbo].[tblCarta] ([IDCarta], [CostoMana], [NombreCarta], [Descripcion], [PV], [Ataque], [Movilidad], [tipo]) VALUES (28, 4, N'Capitan_Pescado', N'El capitan mas guapo de los 7 mares y el único con cara de pescado. Acaso no es curioso?', 5, 5, 5, N'Agua')
INSERT [dbo].[tblCarta] ([IDCarta], [CostoMana], [NombreCarta], [Descripcion], [PV], [Ataque], [Movilidad], [tipo]) VALUES (29, 3, N'Ciber-Perro', N'Para no tener nariz, tiene un gran olfato', 3, 4, 6, N'Espacial')
INSERT [dbo].[tblCarta] ([IDCarta], [CostoMana], [NombreCarta], [Descripcion], [PV], [Ataque], [Movilidad], [tipo]) VALUES (30, 10, N'Corazon_Roto', N'Esa cosa ni sentimientos tiene. No existe una pizca de luz ahi', 10, 15, 7, N'Oscuro')
INSERT [dbo].[tblCarta] ([IDCarta], [CostoMana], [NombreCarta], [Descripcion], [PV], [Ataque], [Movilidad], [tipo]) VALUES (31, 3, N'Cubito_Fuego', N'¿Pensaste que solo el agua podía formar un cubo?', 5, 3, 4, N'Fuego')
INSERT [dbo].[tblCarta] ([IDCarta], [CostoMana], [NombreCarta], [Descripcion], [PV], [Ataque], [Movilidad], [tipo]) VALUES (32, 1, N'Dodo_futuro', N'Cientificos del futuro lograron revivir a los dodos ...No les sirvio de mucho', 1, 1, 3, N'espacial')
INSERT [dbo].[tblCarta] ([IDCarta], [CostoMana], [NombreCarta], [Descripcion], [PV], [Ataque], [Movilidad], [tipo]) VALUES (33, 2, N'Espada_Pez', N'Lo debieron haber puesto pez feo', 2, 5, 2, N'Agua')
INSERT [dbo].[tblCarta] ([IDCarta], [CostoMana], [NombreCarta], [Descripcion], [PV], [Ataque], [Movilidad], [tipo]) VALUES (34, 2, N'Fantasma_amigable', N'No te preocupes por Gary. No mataría ni a una mosca', 2, 3, 4, N'Oscuro')
INSERT [dbo].[tblCarta] ([IDCarta], [CostoMana], [NombreCarta], [Descripcion], [PV], [Ataque], [Movilidad], [tipo]) VALUES (35, 4, N'Flamer', N'No nada mas toxico que un flamer', 4, 7, 5, N'Inestable')
INSERT [dbo].[tblCarta] ([IDCarta], [CostoMana], [NombreCarta], [Descripcion], [PV], [Ataque], [Movilidad], [tipo]) VALUES (36, 2, N'GroselFo', N'Las palabras duelen mas que una daga', 2, 4, 3, N'Magia')
INSERT [dbo].[tblCarta] ([IDCarta], [CostoMana], [NombreCarta], [Descripcion], [PV], [Ataque], [Movilidad], [tipo]) VALUES (37, 3, N'Hombre_Cabra', N'Podra ser una cabra, pero sabe mucho de negocios', 4, 4, 4, N'Naturalia')
INSERT [dbo].[tblCarta] ([IDCarta], [CostoMana], [NombreCarta], [Descripcion], [PV], [Ataque], [Movilidad], [tipo]) VALUES (38, 3, N'Hombre_Nuclear', N'Alguien penso que saltar a un barril lleno de residuo nuclear era buena idea.  ¡No lo es!', 3, 5, 4, N'Inestable')
INSERT [dbo].[tblCarta] ([IDCarta], [CostoMana], [NombreCarta], [Descripcion], [PV], [Ataque], [Movilidad], [tipo]) VALUES (39, 1, N'Huevo_Antiguo_Gigante', N'No quiero saber que pasa si esto te cae encima', 1, 7, 1, N'Reliquia')
INSERT [dbo].[tblCarta] ([IDCarta], [CostoMana], [NombreCarta], [Descripcion], [PV], [Ataque], [Movilidad], [tipo]) VALUES (40, 1, N'Llamita', N'Es tan pequeña que una brisa puede ser fatal ¿Acaso no es una ternura?', 1, 6, 4, N'Fuego')
INSERT [dbo].[tblCarta] ([IDCarta], [CostoMana], [NombreCarta], [Descripcion], [PV], [Ataque], [Movilidad], [tipo]) VALUES (41, 7, N'Mercurio_Vivo', N'Disponible en todos nuestros oceanos  ¡Esto es real hijo!', 4, 10, 10, N'Inestable')
INSERT [dbo].[tblCarta] ([IDCarta], [CostoMana], [NombreCarta], [Descripcion], [PV], [Ataque], [Movilidad], [tipo]) VALUES (42, 5, N'Oso_Armado', N'Odia el bosque asi que decidio talarlo', 7, 7, 3, N'Naturalia')
INSERT [dbo].[tblCarta] ([IDCarta], [CostoMana], [NombreCarta], [Descripcion], [PV], [Ataque], [Movilidad], [tipo]) VALUES (43, 6, N'Profesora_Historia', N'Es facil dar clases de historia si viviste la historia. Cuidado con Bethy, al minimo descuido te reprobara sin comapsion. Su corazon se perdio junto a la Atlantida', 7, 9, 4, N'Reliquia')
INSERT [dbo].[tblCarta] ([IDCarta], [CostoMana], [NombreCarta], [Descripcion], [PV], [Ataque], [Movilidad], [tipo]) VALUES (44, 2, N'QTezal', N'Animal Mitico. \nAntigias culturas pensaban que traia buena suerte', 1, 2, 5, N'Reliquia')
INSERT [dbo].[tblCarta] ([IDCarta], [CostoMana], [NombreCarta], [Descripcion], [PV], [Ataque], [Movilidad], [tipo]) VALUES (45, 5, N'Robo -Abeja', N'Salven a las abejas, ellas son el futuro ¿Esperaban un chiste? El mundo no es asi', 4, 7, 8, N'Espacial')
INSERT [dbo].[tblCarta] ([IDCarta], [CostoMana], [NombreCarta], [Descripcion], [PV], [Ataque], [Movilidad], [tipo]) VALUES (46, 3, N'Televisor_Danado', N'Sintoniza mas que 10000 canales. Sin nada bueno que ver...', 6, 2, 2, N'Oscuro')
INSERT [dbo].[tblCarta] ([IDCarta], [CostoMana], [NombreCarta], [Descripcion], [PV], [Ataque], [Movilidad], [tipo]) VALUES (47, 7, N'Tren_Arcoiris', N'El metodo de transporte mas colorido del mercado', 7, 5, 10, N'Magia')
SET IDENTITY_INSERT [dbo].[tblCarta] OFF
SET IDENTITY_INSERT [dbo].[tblusuario] ON 

INSERT [dbo].[tblusuario] ([ID], [Nombre], [NombreUsuario], [Correo], [passwd]) VALUES (28, N'M', N'Pp', N'alsO@', N'b')
SET IDENTITY_INSERT [dbo].[tblusuario] OFF
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__tblusuar__60695A1967348B77]    Script Date: 3/6/2019 7:31:40 ******/
ALTER TABLE [dbo].[tblusuario] ADD UNIQUE NONCLUSTERED 
(
	[Correo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__tblusuar__6B0F5AE0150FAA2E]    Script Date: 3/6/2019 7:31:40 ******/
ALTER TABLE [dbo].[tblusuario] ADD UNIQUE NONCLUSTERED 
(
	[NombreUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
ALTER TABLE [dbo].[tblBaraja]  WITH CHECK ADD FOREIGN KEY([ID_Usuario])
REFERENCES [dbo].[tblusuario] ([ID])
GO
ALTER TABLE [dbo].[tblBarajaCarta]  WITH CHECK ADD FOREIGN KEY([CodBaraja])
REFERENCES [dbo].[tblBaraja] ([CodBaraja])
GO
ALTER TABLE [dbo].[tblBarajaCarta]  WITH CHECK ADD FOREIGN KEY([IDCarta])
REFERENCES [dbo].[tblCarta] ([IDCarta])
GO
ALTER TABLE [dbo].[tblPartida]  WITH CHECK ADD FOREIGN KEY([ID_Usuario])
REFERENCES [dbo].[tblusuario] ([ID])
GO
